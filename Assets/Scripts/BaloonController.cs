using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonController : MonoBehaviour
{
    public GameObject pop;
    
    UIController ui;
    int count = 0;
    float score;

  
        // Start is called before the first frame update
    void Start()
    {
        ui = GameObject.Find("GameController").GetComponent<UIController>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void FixedUpdate(){
        Destroy(gameObject,6f);
    }
    void OnMouseDown(){
        
       GameObject popClone = Instantiate(pop,transform.position,transform.rotation);
        Destroy(gameObject);
        ui.Counter();
        Destroy(popClone,0.3f);
        
    }
   
}
