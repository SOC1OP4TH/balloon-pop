using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LevelPanel : MonoBehaviour
{
    public GameObject levelButtonPrefab;   // The prefab for the level button to be created
    public RectTransform levelsPanel;      // The panel that will hold the level buttons

    private void Start()
    {
        // Get the number of levels from somewhere (e.g. a save file)
        int numLevels = 5;
        // Create a level button for each level
        for (int i = 1; i <= numLevels; i++)
        {
            // Instantiate the level button prefab
            GameObject levelButtonGO = Instantiate(levelButtonPrefab, levelsPanel);
            // Set the text of the button to the level number
            Text buttonText = levelButtonGO.GetComponentInChildren<Text>();
            buttonText.text = "Level" + i;
            // Add an onClick event to the button
            Button button = levelButtonGO.GetComponent<Button>();
            int levelIndex = i;   
            // Capture the level index in a local variable to avoid closure issues
            button.onClick.AddListener(() => LoadLevel(levelIndex));
            RectTransform buttonRectTransform = levelButtonGO.GetComponent<RectTransform>();
            buttonRectTransform.anchoredPosition = new Vector2(0f, (-(buttonRectTransform.sizeDelta.y)-i*60));
        }
    }
    private void LoadLevel(int levelIndex)
    {
        SceneManager.LoadScene("Level" + levelIndex);
    }
}
