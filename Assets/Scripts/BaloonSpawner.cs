using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaloonSpawner : MonoBehaviour
{
    public GameObject baloon;
    public GameObject parent;
    float spawnTime = 1f;
    float timer = 0f;
    int countOfBaloons;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Spawn();
    }
    void Spawn()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            Vector3 pos = new Vector3(Random.Range(-2f, 3f), Random.Range(-1f, 1f), 1);
            Quaternion quaternion = new Quaternion(0, 0, 0, 0);

            GameObject cloneBaloon = Instantiate(baloon, pos, quaternion);
            cloneBaloon.transform.parent = parent.transform;

            cloneBaloon.GetComponent<Rigidbody2D>().AddForce(new Vector3(0, Random.Range(20f, 50f), 0));
            timer = spawnTime;
        }
        
    }
}
