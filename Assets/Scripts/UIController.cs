using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class UIController : MonoBehaviour
{
    public GameObject spawner;
    public int goal;
    public Button Restart;
    public Button MainMenu;
    public Button NextLevel;
    public Text info;
    public Text timer;
    public Text score;
 
    bool isDone;
    [SerializeField]
    float time ;
    int count = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        Restart.gameObject.SetActive(false);
        MainMenu.gameObject.SetActive(false);
        NextLevel.gameObject.SetActive(false);
        info.text = $"You Should POP at least {goal} baloons";
        Invoke("HideText",2f);
        score.text = "Count: "+ count;    
    }
    // Update is called once per frame
    void Update()
    {
        WinController();
        if(!isDone){
        Timer();  
        }
    }
    void Timer(){
          if(time>0){
        time -= Time.deltaTime;
        timer.text = "Time: "+(int)time;
        }
    }
    void WinController(){
        
        if(time<=0){
        spawner.GetComponent<BaloonSpawner>().enabled= false;
        LoseButtons();
        timer.text = "Time: 0";
        info.enabled= true;
        info.text = "Game Over";
        }
        if(count>=goal){
            spawner.GetComponent<BaloonSpawner>().enabled= false;
            WinButtons();
            info.enabled= true;
            info.text = "You Win";
            isDone = true;
        }
    }
    void WinButtons(){
           LoseButtons();
            NextLevel.gameObject.SetActive(true);
    }
    void LoseButtons(){
            Restart.gameObject.SetActive(true);
            MainMenu.gameObject.SetActive(true);
    }
    public void Counter(){
        count++;
        score.text = "Count: "+ count;
    }
    void HideText(){
        info.enabled = false;
    }
    public void RestartGame(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void MainMenuGame(){
        SceneManager.LoadScene("Menu");
    }
    public void NextLevelGame(){
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(currentScene);
        SceneManager.LoadScene(currentScene+1);
    }



}
