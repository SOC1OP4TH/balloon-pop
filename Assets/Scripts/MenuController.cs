using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuController : MonoBehaviour
{

    // Start is called before the first frame update
  
    public Button quit;
    public Button play;
    public Button levels;

  public void Quit(){
        Application.Quit();
  }
    public void Play(){
        SceneManager.LoadScene("Level1");
    }
    public void Levels(){
        SceneManager.LoadScene("Levels");
    }

}
